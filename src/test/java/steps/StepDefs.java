package steps;

import cucumber.api.Scenario;
import cucumber.api.java.After;
import cucumber.api.java.Before;
import cucumber.api.java.en.Given;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;
import org.openqa.selenium.OutputType;
import org.openqa.selenium.TakesScreenshot;
import org.openqa.selenium.WebDriver;
import org.testng.asserts.SoftAssert;
import pages.HomePage;
import pages.SampleStorePage;
import pages.ShoppingCartPage;
import utils.BaseUtils;
import utils.HelperUtils;


public class StepDefs extends BaseUtils {

    private static SoftAssert _softAssert = new SoftAssert();
    private WebDriver driver;
    private HelperUtils helperUtils;
    private HomePage homePage;
    private ShoppingCartPage shoppingCartPage;
    private SampleStorePage sampleStorePage;
    private Scenario _scenario;

    @Before
    public void initializeScenario(Scenario scenarioInstance) {
        this._scenario = scenarioInstance;
        driver = getDriver();
        homePage = new HomePage(driver);
        helperUtils = new HelperUtils(driver);
    }

    @Given("^User navigated to midtrans home page$")
    public void user_navigated_to_midtrans_home_page() {
        _softAssert.assertAll();
        _softAssert.assertTrue(homePage.verifyHomePageNavigation(), "User is unable to navigate to home page.");
    }

    @Then("^User clicks on Buy Now button$")
    public void user_clicks_on_Buy_Now_button() {
        shoppingCartPage = homePage.clickOnBuyNowButton();
    }

    @Then("^User enters personal details \"([^\"]*)\" \"([^\"]*)\" \"([^\"]*)\" \"([^\"]*)\" \"([^\"]*)\" \"([^\"]*)\"$")
    public void user_enters_personal_details(String name, String email, String phone, String city, String address, String postcode) {
        shoppingCartPage.enterUserDetails(name, email, phone, city, address, postcode);
    }

    @When("^User clicks on Checkout button$")
    public void user_clicks_on_Checkout_button() {
        sampleStorePage = shoppingCartPage.clickOnCheckoutButton();
        helperUtils.switchToFrame(sampleStorePage._frameID);
    }

    @Then("^User summary popup opens$")
    public void user_summary_popup_opens() {
        _softAssert.assertTrue(sampleStorePage.verifySummaryPopUp(), "User is unable to navigate to summary popup.");
        _softAssert.assertAll();
    }

    @Then("^Verify order details \"([^\"]*)\"$")
    public void verifyOrderDetails(String amount) {
        _softAssert.assertTrue(sampleStorePage.verifyOrderDetails(amount), "Order amount did not match.");
        _softAssert.assertAll();
    }

    @Then("^Verify shipping details \"([^\"]*)\" \"([^\"]*)\" \"([^\"]*)\" \"([^\"]*)\"$")
    public void verifyShippingDetails(String name, String email, String phone, String address) {
        _softAssert.assertTrue(sampleStorePage.verifyShippingDetails(name, email, phone, address), "Shipping details did not match.");
        _softAssert.assertAll();
    }

    @When("^User clicks on Continue Button$")
    public void user_clicks_on_Continue_Button() {
        sampleStorePage.clickOnContinueButton();
    }

    @Then("^User can view different payment options$")
    public void user_can_view_different_payment_options() {
        _softAssert.assertTrue(sampleStorePage.verifyPaymentModePage(), "User is unable to navigate to payment mode page.");
        _softAssert.assertAll();
    }

    @When("^User clicks on Credit Card$")
    public void user_clicks_on_Credit_Card() {
        sampleStorePage.clickOnCreditCardOption();
    }

    @Then("^User can view credit card detail section$")
    public void user_can_view_credit_card_detail_section() {
        _softAssert.assertTrue(sampleStorePage.verifyCreditCardDetailPage(), "User is unable to navigate to card detail page.");
        _softAssert.assertAll();
    }

    @Then("^User enters card details \"([^\"]*)\" \"([^\"]*)\" \"([^\"]*)\"$")
    public void user_enters_card_details(String cardNumber, String expiryDate, String cvvNumber) {
        sampleStorePage.enterCardDetails(cardNumber, expiryDate, cvvNumber);
    }

    @When("^User clicks on Pay Now button$")
    public void user_clicks_on_Pay_Now_button() {
        _softAssert.assertTrue(sampleStorePage.clickOnPayNowButton(), "User is unable to navigate to OTP screen.");
        _softAssert.assertAll();
    }

    @Then("^User can view the Bank's OTP Screen$")
    public void user_can_view_the_Bank_s_OTP_Screen() throws InterruptedException {
        helperUtils.switchToFrame(sampleStorePage._paymentFrame);
        _softAssert.assertTrue(sampleStorePage.verifyOTPScreen(), "User is unable to navigate to OTP screen.");
        _softAssert.assertAll();
    }

    @Then("^User enters \"([^\"]*)\" and clicks on ok button$")
    public void user_enters_and_clicks_on_ok_button(String otp) {
        sampleStorePage.enterOtpAndClickOnOkButton(otp);
    }

    @Then("^User should see the successful transaction screen$")
    public void user_should_see_the_successful_transaction_screen() {
        helperUtils.switchToDefaultFrame();
        helperUtils.switchToFrame(sampleStorePage._frameID);
        _softAssert.assertTrue(sampleStorePage.verifySuccessfulTransaction(), "Transaction failed.");
        helperUtils.switchToDefaultFrame();
        _softAssert.assertAll();
    }

    @Then("^User should navigate to home page screen$")
    public void user_should_navigate_to_home_page_screen() {
        _softAssert.assertTrue(sampleStorePage.verifySuccessfulHomePageNavigation(), "User is unable to navigate to home page.");
        _softAssert.assertAll();
    }

    @After
    public void close_All_Browser_Instances() {
        if (_scenario.isFailed()) {
            _scenario.embed(((TakesScreenshot) driver).getScreenshotAs(OutputType.BYTES), "image/png");
            _scenario.write("Scenario Failed !!");
        } else {
            _scenario.write("Scenario Passed !!");
        }

        if (driver != null) {
            driver.manage().deleteAllCookies();
            driver.close();
        }
    }

}
