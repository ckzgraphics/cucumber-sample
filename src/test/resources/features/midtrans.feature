Feature: Midtrans Payment Flow
    As a user
    I want to purchase the pillow using credit card

    @sanity
    Scenario Outline:
        Given User navigated to midtrans home page
        Then User clicks on Buy Now button
        Then User enters personal details "<Name>" "<Email>" "<PhoneNumber>" "<City>" "<Address>" "<PostalCode>"
        When User clicks on Checkout button
        Then User summary popup opens
        Then Verify order details "<Amount>"
        Then Verify shipping details "<Name>" "<Email>" "<PhoneNumber>" "<Address>"
        When User clicks on Continue Button
        Then User can view different payment options
        When User clicks on Credit Card
        Then User can view credit card detail section
        Then User enters card details "<CardNumber>" "<ExpiryDate>" "<CVVNumber>"
        When User clicks on Pay Now button
        Then User can view the Bank's OTP Screen
        Then User enters "<OTP>" and clicks on ok button
        Then User should see the successful transaction screen
        Then User should navigate to home page screen

        Examples:
            | Name | Email | PhoneNumber | City | Address | PostalCode | Amount | CardNumber | ExpiryDate | CVVNumber | OTP |
            | Andre Tarkovsky | andre.tark@go.com | 6756218766 | Pune | Wakad | 2000 | 20,000 | 4811111111111114 | 02/20 | 123 | 112233 |
            | Ingmar Bergman | ingmar.berg@go.com | 6756218765 | Pune | Wakad | 2000 | 20,000 | 4811111111111113 | 02/20 | 123 | 112233 |