package pages;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import utils.BaseUtils;
import utils.HelperUtils;

public class HomePage extends BaseUtils {
    private WebDriver driver;
    private HelperUtils helperUtils;

    private By _title = By.xpath("//div[@class='title']");
    private By _buyNowButton = By.xpath("//a[text()='BUY NOW']");

    public HomePage(WebDriver driver){
        this.driver = driver;
        helperUtils = new HelperUtils(driver);
    }

    public Boolean verifyHomePageNavigation(){
        return helperUtils.verifyTextMatch(helperUtils.getText(_title), "Midtrans Pillow");
    }

    public ShoppingCartPage clickOnBuyNowButton(){
        helperUtils.click(_buyNowButton);
        return new ShoppingCartPage(driver);
    }
}
