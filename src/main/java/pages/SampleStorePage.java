package pages;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import utils.BaseUtils;
import utils.HelperUtils;

import java.util.ArrayList;

public class SampleStorePage extends BaseUtils {

    private WebDriver driver;
    private HelperUtils helperUtils;

    public String _frameID = "snap-midtrans";
    public By _paymentFrame = By.xpath("//iframe[contains(@src,'veritrans')]");
    private By _sectionTitle = By.xpath("//p[@class='text-page-title-content']");
    private By _orderAmountDetail = By.xpath("//td[@class='table-amount text-body']");
    private By _shippingDetailLink = By.xpath("//span[text()='shipping details']");
    private By _name = By.xpath("//div[@class='text-body-title' and text()='Name']//following::div[1]");
    private By _address = By.xpath("//div[@class='text-body-title' and text()='Address']//following::div[1]");
    private By _phone = By.xpath("//div[@class='text-body-title' and text()='Phone number']//following::div[1]");
    private By _email = By.xpath("//div[@class='text-body-title' and text()='Email']//following::div[1]");
    private By _continueButton = By.xpath("//a[contains(@href, 'select-payment')]");
    private By _creditCardLink = By.xpath("//a[contains(@href, 'credit-card')]");
    private By _cardNumber = By.name("cardnumber");
    private By _expiryDate = By.xpath("//input[@placeholder='MM / YY']");
    private By _cvvNmber = By.xpath("//input[@placeholder='123']");
    private By _payNowButton = By.className("button-main-content");
    private By _invalidCardError = By.xpath("//span[text()='Invalid card number']");
    private By _otpField = By.xpath("//input[@id='PaRes']");
    private By _okButton = By.name("ok");
    private By _transactionConfirmation = By.xpath("//div[text()='Transaction successful']");
    private By _buyNowButton = By.xpath("//a[text()='BUY NOW']");


    public SampleStorePage(WebDriver driver){
        this.driver = driver;
        helperUtils = new HelperUtils(driver);
    }

    public Boolean verifySummaryPopUp(){
        return helperUtils.verifyTextMatch(helperUtils.getText(_sectionTitle), "Order Summary");
    }

    public Boolean verifyOrderDetails(String amount){
        String actualAmount = helperUtils.getText(_orderAmountDetail);
        return helperUtils.verifyTextMatch(actualAmount, amount);
    }

    public Boolean verifyShippingDetails(String name, String email, String phone, String address){
        helperUtils.click(_shippingDetailLink);

        boolean flag = false;
        ArrayList<Boolean> results = new ArrayList<Boolean>();

        String actualName = helperUtils.getText(_name);
        String actualEmail = helperUtils.getText(_email);
        String actualPhone = helperUtils.getText(_phone);
        String actualAddress = helperUtils.getText(_address);

        results.add(helperUtils.verifyTextMatch(actualName, name));
        results.add(helperUtils.verifyTextMatch(actualEmail, email));
        results.add(helperUtils.verifyTextMatch(actualPhone, phone));
        results.add(helperUtils.verifyTextContains(actualAddress, address));

        for(Boolean bool: results) {
            if(!bool){
                flag = false;
                break;
            }else {
                flag = true;
            }
        }

        return flag;
    }

    public void clickOnContinueButton(){
        helperUtils.click(_continueButton);
    }

    public Boolean verifyPaymentModePage(){
        return helperUtils.verifyTextMatch(helperUtils.getText(_sectionTitle), "Select Payment");
    }

    public void clickOnCreditCardOption(){
        helperUtils.click(_creditCardLink);
    }

    public Boolean verifyCreditCardDetailPage(){
        return helperUtils.verifyTextMatch(helperUtils.getText(_sectionTitle), "Credit Card");
    }

    public void enterCardDetails(String cardNumber, String expiryDate, String cvvNmber){
        helperUtils.enterText(_cardNumber, cardNumber);
        helperUtils.enterText(_expiryDate, expiryDate);
        helperUtils.enterText(_cvvNmber, cvvNmber);
    }

    public Boolean clickOnPayNowButton(){
        boolean flag = false;
        helperUtils.click(_payNowButton);

        if (helperUtils.isElementInvisible(_cardNumber) && helperUtils.isElementInvisible(_invalidCardError)){
            flag = true;
        }else{
            logger.error("User is unable to navigate to OTP screen.");
        }
        return flag;
    }

    public Boolean verifyOTPScreen() throws InterruptedException {
        Thread.sleep(7000);
        return helperUtils.isElementDisplayed(_otpField);
    }

    public void enterOtpAndClickOnOkButton(String otp) {
        helperUtils.enterText(_otpField, otp);
        helperUtils.click(_okButton);
    }

    public Boolean verifySuccessfulTransaction(){
        return helperUtils.isElementDisplayed(_transactionConfirmation);
    }

    public Boolean verifySuccessfulHomePageNavigation(){
        return helperUtils.isElementDisplayed(_buyNowButton);
    }

}
