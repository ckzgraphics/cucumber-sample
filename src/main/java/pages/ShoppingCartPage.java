package pages;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import utils.BaseUtils;
import utils.HelperUtils;

public class ShoppingCartPage extends BaseUtils {
    private WebDriver driver;
    private HelperUtils helperUtils;

    private By _name = By.xpath("//td[text()='Name']//following::input[1]");
    private By _email = By.xpath("//td[text()='Email']//following::input[1]");
    private By _phone = By.xpath("//td[text()='Phone no']//following::input[1]");
    private By _city = By.xpath("//td[text()='City']//following::input[1]");
    private By _address = By.xpath("//td[text()='Address']//following::textarea[1]");
    private By _postcode = By.xpath("//td[text()='Postal Code']//following::input[1]");
    private By _checkoutButton = By.xpath("//div[text()='CHECKOUT']");

    public ShoppingCartPage(WebDriver driver){
        this.driver = driver;
        helperUtils = new HelperUtils(driver);
    }

    public void enterUserDetails(String name, String email, String phone,
                                 String city, String address, String postcode){
        helperUtils.enterText(_name, name);
        helperUtils.enterText(_email, email);
        helperUtils.enterText(_phone, phone);
        helperUtils.enterText(_city, city);
        helperUtils.enterText(_address, address);
        helperUtils.enterText(_postcode, postcode);
    }

    public SampleStorePage clickOnCheckoutButton(){
        helperUtils.click(_checkoutButton);
        return new SampleStorePage(driver);
    }
}
