package utils;

import io.github.bonigarcia.wdm.ChromeDriverManager;
import io.github.bonigarcia.wdm.FirefoxDriverManager;
import org.apache.log4j.Level;
import org.apache.log4j.Logger;
import org.apache.log4j.PropertyConfigurator;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.chrome.ChromeOptions;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.firefox.FirefoxOptions;
import org.openqa.selenium.remote.DesiredCapabilities;
import org.openqa.selenium.remote.RemoteWebDriver;
import java.io.FileInputStream;
import java.net.MalformedURLException;
import java.net.URL;
import java.text.SimpleDateFormat;
import java.time.Duration;
import java.util.Date;
import java.util.HashMap;
import java.util.Properties;
import java.util.concurrent.TimeUnit;

public class BaseUtils {

    static {
        SimpleDateFormat dateFormat = new SimpleDateFormat("yyyyMMdd_hhmmss");
        System.setProperty("current.date", dateFormat.format(new Date()));
    }

    private Properties props;
    private WebDriver driver;
    public Logger logger;

    public BaseUtils() {
        props = new Properties();
        String configPath = System.getProperty("user.dir") + "/src/main/resources/config.properties";
        try {
            FileInputStream inputStream = new FileInputStream(configPath);
            props.load(inputStream);
            PropertyConfigurator.configure(System.getProperty("user.dir") + "/src/main/resources/log4j.properties");
            logger = Logger.getLogger(BaseUtils.class.getName());
            logger.setLevel(Level.INFO);
        } catch (Exception Ex) {
            Ex.printStackTrace();
        }
    }

    String getProperty(String property) {
        return props.getProperty(property);
    }

    protected WebDriver getDriver() {
        String browser = getProperty("BROWSER");
        String environment = getProperty("ENVIRONMENT");
        boolean headless = Boolean.parseBoolean(getProperty("HEADLESS"));
        Duration TIMEOUT = Duration.ofSeconds(Integer.parseInt(getProperty("TIMEOUT")));

        switch (environment) {
            case "local":
                switch (browser) {
                    case "chrome":
                        ChromeDriverManager.chromedriver().setup();
                        ChromeOptions chromeOptions = new ChromeOptions();
                        chromeOptions.addArguments("disable-infobars");
                        chromeOptions.setHeadless(headless);
                        driver = new ChromeDriver(chromeOptions);
                        driver.manage().window().maximize();
                        driver.manage().timeouts().pageLoadTimeout(TIMEOUT.getSeconds(), TimeUnit.SECONDS);
                        break;

                    case "firefox":
                        FirefoxDriverManager.firefoxdriver().setup();
                        driver = new FirefoxDriver();
                        break;
                }
                break;

            case "grid":
                switch (browser) {
                    case "chrome":
                        ChromeOptions chromeOptions = new ChromeOptions();
                        chromeOptions.addArguments("disable-infobars");
                        chromeOptions.setHeadless(headless);
                        chromeOptions.setCapability("platform", getProperty("GRID_PLATFORM"));
                        try {
                            driver = new RemoteWebDriver(new URL(getProperty("GRID_SERVER")), chromeOptions);
                        } catch (MalformedURLException Ex) {
                            logger.error("Exception Occurred: \n" + Ex.getMessage());
                        }
                        break;

                    case "firefox":
                        FirefoxOptions firefoxOptions = new FirefoxOptions();
                        try {
                            driver = new RemoteWebDriver(new URL(getProperty("GRID_SERVER")), firefoxOptions);
                        } catch (MalformedURLException Ex) {
                            logger.error("Exception Occurred: \n" + Ex.getMessage());
                        }
                        break;
                }
                break;

            case "browserStack":

                String url = String.format("http://%s:%s@hub.browserstack.com:80/wd/hub",
                        getProperty("BS_USERNAME"), getProperty("BS_KEY"));

                DesiredCapabilities caps = new DesiredCapabilities();
                caps.setCapability("browserName", getProperty("BROWSER_NAME"));
                caps.setCapability("browserVersion", getProperty("BROWSER_VERSION"));

                HashMap<String, Object> bsOptions = new HashMap<>();
                bsOptions.put("os", getProperty("OS"));
                bsOptions.put("osVersion", getProperty("OS_VERSION"));
                bsOptions.put("projectName", getProperty("PROJECT_NAME"));
                bsOptions.put("buildName", getProperty("BUILD_NAME"));
                bsOptions.put("sessionName", getProperty("SESSION_NAME"));
                bsOptions.put("local", Boolean.parseBoolean(getProperty("LOCAL")));
                bsOptions.put("debug", Boolean.parseBoolean(getProperty("DEBUG")));
                bsOptions.put("consoleLogs", getProperty("CONSOLE_LOGS"));
                bsOptions.put("networkLogs", Boolean.parseBoolean(getProperty("NETWORK_LOGS")));

                caps.setCapability("bstack:options", bsOptions);

                try {
                    driver = new RemoteWebDriver(new URL(url), caps);
                } catch (MalformedURLException Ex) {
                    logger.error("Exception Occurred: \n" + Ex.getMessage());
                }
        }
        driver.get(getProperty("URL"));
        logger.info(String.format("Starting test execution on %s - %s.", browser, environment));
        return driver;
    }

}
