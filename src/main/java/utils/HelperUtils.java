package utils;

import org.apache.commons.io.FileUtils;
import org.apache.log4j.Logger;
import org.openqa.selenium.*;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;
import java.io.File;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.Date;

public class HelperUtils extends BaseUtils{

    private WebDriver driver;
    private WebDriverWait wait;

    public HelperUtils(WebDriver driver) {
        this.driver = driver;
        long TIMEOUT = Integer.parseInt(getProperty("TIMEOUT"));
        long POLLING = Integer.parseInt(getProperty("POLLING"));
        wait = new WebDriverWait(driver, TIMEOUT, POLLING);
        logger = Logger.getLogger(HelperUtils.class.getName());
    }

    private void waitForElementToAppear(By locator) {
        wait.until(ExpectedConditions.visibilityOfElementLocated(locator));
    }

    private void waitForElementToDisappear(By locator) {
        wait.until(ExpectedConditions.invisibilityOfElementLocated(locator));
    }

    public Boolean isElementDisplayed(By locator){
        boolean flag = false;
        try{
            waitForElementToAppear(locator);
            flag = true;
            logger.info("Element displayed:: "+locator.toString());
        }catch(Exception Ex){
            logger.error("Element not displayed:: "+locator.toString());
        }
        return flag;
    }

    public Boolean isElementInvisible(By locator){
        boolean flag = false;
        try{
            waitForElementToDisappear(locator);
            flag = true;
            logger.info("Element is invisible now:: "+locator.toString());
        }catch(Exception Ex){
            logger.error("Element is still displayed:: "+locator.toString());
        }
        return flag;
    }

    public void click(By locator) {
        try {
            waitForElementToAppear(locator);
            driver.findElement(locator).click();
            logger.info("Clicking on element:: "+locator.toString());
        }catch (Exception Ex){
            logger.error("Element not visible or clickable:: "+locator.toString());
            throw new AssertionError("Element not visible or clickable:: "+locator.toString());
        }

    }

    public void enterText(By locator, String value) {
        try{
            waitForElementToAppear(locator);
            WebElement element = driver.findElement(locator);
            element.clear();
            element.sendKeys(value);
            logger.info("Entering text to element:: "+locator.toString());
        }catch (Exception Ex){
            logger.error("Unable to enter text to the element:: "+locator.toString());
            throw new AssertionError("Unable to enter text to the element:: "+locator.toString());
        }
    }

    public String getText(By locator) {
        String text = null;
        try{
            waitForElementToAppear(locator);
            text = driver.findElement(locator).getText();
            logger.info("Getting inner text from element:: "+locator.toString());
        }catch(Exception Ex){
            logger.error("Unable to get text from the element:: "+locator.toString());
            throw new AssertionError("Unable to get text from the element:: "+locator.toString());
        }
        return text;
    }

    public boolean verifyTextMatch(String actualText, String expectedText){
        boolean flag = false;
        try {
            logger.info("Actual text:: " + actualText);
            logger.info("Expected text:: " + expectedText);

            if(actualText.equals(expectedText)){
                flag = true;
                logger.info("### TEXT MATCHED.");
            }else{
                logger.error("### TEXT DOES NOT MATCHED.");
            }

        }catch (Exception Ex){
            logger.error("Exception Occurred While Verifying The Text Match:: " + Ex.getMessage());
        }
        return flag;
    }

    public boolean verifyTextContains(String actualText, String expectedText){
        boolean flag = false;
        try {
            logger.info("Actual text:: " + actualText);
            logger.info("Expected text to be Present in Actual text --> :: " + expectedText);

            if(actualText.contains(expectedText)){
                flag = true;
                logger.info("### ACTUAL TEXT CONTAINS EXPECTED TEXT !!!");
            }else{
                logger.error("### ACTUAL TEXT DOES NOT CONTAINS EXPECTED TEXT !!!");
            }

        }catch (Exception Ex){
            logger.error("Exception Occurred While Verifying The Text Match: " + Ex.getMessage());
        }
        return flag;
    }

    public void hoverAndClick(By locator) {
        try{
            waitForElementToAppear(locator);
            Actions actions = new Actions(driver);
            actions.moveToElement(driver.findElement(locator)).click();
            logger.info("Clicking on element:: "+locator.toString());
        }catch(Exception Ex){
            logger.error("Element not visible or clickable:: "+locator.toString());
            throw new AssertionError("Element not visible or clickable:: "+locator.toString());
        }

    }

    public void hoverAndClick(By locator, By targetLocator) {
        try{
            waitForElementToAppear(locator);
            Actions actions = new Actions(driver);
            actions.moveToElement(driver.findElement(locator)).
                    click(driver.findElement(targetLocator)).perform();
            logger.info("Clicking on element:: "+targetLocator.toString());
        }catch(Exception Ex){
            logger.error("Element not visible or clickable.\n"+locator.toString());
            throw new AssertionError("Element not visible or clickable.\n"+locator.toString());
        }
    }

    public void switchToFrame(String frameId){
        try{
            driver.switchTo().frame(frameId);
            logger.info(String.format("Switched to frame:: %s", frameId));
        }catch(Exception Ex){
            logger.error(String.format("Exception occurred while to switching to the frame - %s.\n%s",
                    frameId, Ex.getMessage()));
        }
    }

    public void switchToFrame(By locator){
        try{
            waitForElementToAppear(locator);
            WebElement _frameElement = driver.findElement(locator);
            driver.switchTo().frame(_frameElement);
            logger.info(String.format("Switched to frame by locator:: %s", locator.toString()));
        }catch(Exception Ex){
            logger.error(String.format("Exception occurred while switching to frame by locator - %s.\n%s",
                    locator.toString(), Ex.getMessage()));
        }
    }

    public void switchToDefaultFrame(){
        try{
            driver.switchTo().defaultContent();
            logger.info("Switched to parent frame.");
        }catch(Exception Ex){
            logger.error("Exception occurred while switching to parent frame.\n"+Ex.getMessage());
        }
    }

    public String takeScreenshot(String screenshotName){
        var screenshotDir = System.getProperty("user.dir")+"/execution-results/screenshots/";
        String dateName = new SimpleDateFormat("yyyyMMdd_hhmmss").format(new Date());
        var capture = (TakesScreenshot) driver;

        var source = capture.getScreenshotAs(OutputType.FILE);
        var target = new File(screenshotDir+screenshotName+"_"+dateName+".png");

        try {
            FileUtils.copyFile(source, target);
        } catch (IOException Ex) {
            logger.error("Exception occurred while taking the screenshot.\n" + Ex.getMessage());
        }
        return screenshotDir;
    }
}
