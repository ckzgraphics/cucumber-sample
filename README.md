## Cucumber BDD Framework

- Simply run ``` mvn clean install``` command to run test cases

> #### Major Files

- **pages:** all page objects can be found here
    
    ```/cucumber-sample/src/main/java/pages```

- **utils:** all base and helper utils can be found here
    
    ```/cucumber-sample/src/main/java/utils```     

- **config.properties:** file to change global configuration like browser, url, timeout and grid settings 
    
    ```/cucumber-sample/src/main/resources/config.properties```

- **midtrans.feature:** feature file containing scenarios 
    
    ```/cucumber-sample/src/test/resources/features/midtrans.feature```

- **StepDefs.java:** steps definition for feature file 
    
    ```/cucumber-sample/src/test/java/steps/StepDefs.java```

- **TestRunner.java:** test runner class to execute the step definitions, this file can be changed for different tags to run like - @sanity, @regression 
    
    ```/cucumber-sample/src/test/java/tests/TestRunner.java```

- **Execution Report**

    ```/cucumber-sample/target/cucumber-reports/cucumber-pretty/index.html```

- **Logs**

    ```/cucumber-sample/execution-results/logs```